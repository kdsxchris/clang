Extensions to the Clang C++ compiler.

See [upstream tracking](https://gitlab.com/lock3/clang/wikis/Upstream-Tracking)
for a list of stable HEADs to build this project against.